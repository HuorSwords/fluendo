﻿namespace Fluendo.Pubg.Domain.Contracts.Services
{
    using System.Threading.Tasks;
    using Fluendo.Pubg.Domain.Models;

    public interface ILeaderBoardService
    {
        bool IsValidateGameMode(string gameMode);

        Task<LeaderBoard> GetLeaderBoardByModeAsync(string gameMode);
    }
}