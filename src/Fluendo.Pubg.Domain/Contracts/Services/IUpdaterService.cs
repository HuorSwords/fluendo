﻿namespace Fluendo.Pubg.Domain.Contracts
{
    using System.Threading.Tasks;

    using Fluendo.Pubg.Domain.Models;

    public interface IUpdaterService
    {
        Task<LeaderBoard> RequestOrUpdateLeaderBoardAsync(string gameMode);
    }
}