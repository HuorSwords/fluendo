﻿namespace Fluendo.Pubg.Domain.Contracts.Agents
{
    using System.Threading;
    using System.Threading.Tasks;

    public interface IPubgApiClient<TEntity> where TEntity : class
    {
        /// <summary>
        /// Gets the item in the given <paramref name="path"/>.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>The <see cref="T:Task{TEntity}"/> instance.</returns>
        Task<TEntity> GetAsync(string path);

        /// <summary>
        /// Gets the item in the given <paramref name="path"/>.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>The <see cref="T:Task{TEntity}"/> instance.</returns>
        Task<TEntity> GetAsync(string path, CancellationToken cancellationToken);
    }
}