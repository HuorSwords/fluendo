﻿namespace Fluendo.Pubg.Domain.Contracts.Agents
{
    using System.Threading.Tasks;

    using Fluendo.Pubg.Domain.Models;

    public interface ILeaderBoardPubgAgent : IPubgApiClient<LeaderBoard>
    {
        Task<LeaderBoard> GetTop100PlayersAsync(string gameMode);
    }
}