﻿namespace Fluendo.Pubg.Domain.Contracts.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Fluendo.Pubg.Domain.Models;

    public interface ILeaderBoardRepository
    {
        IEnumerable<string> GetAvailableGameModes();
        Task<LeaderBoard> GetLeaderBoardAsync(string gameMode);
        Task<LeaderBoard> UpdateLeaderBoardAsync(LeaderBoard leaderBoard);
    }
}