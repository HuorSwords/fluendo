﻿namespace Fluendo.Pubg.Domain.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Fluendo.Pubg.Domain.Contracts;
    using Fluendo.Pubg.Domain.Contracts.Repositories;
    using Fluendo.Pubg.Domain.Contracts.Services;
    using Fluendo.Pubg.Domain.Models;

    public class LeaderBoardService : ILeaderBoardService
    {
        private readonly IUpdaterService updaterService;
        private readonly ILeaderBoardRepository leaderBoardRepository;

        public LeaderBoardService(IUpdaterService updaterService, ILeaderBoardRepository leaderBoardRepository)
        {
            this.updaterService = updaterService ?? throw new ArgumentNullException(nameof(updaterService));
            this.leaderBoardRepository = leaderBoardRepository ?? throw new ArgumentNullException(nameof(leaderBoardRepository));
        }

        public async Task<LeaderBoard> GetLeaderBoardByModeAsync(string gameMode)
        {
            LeaderBoard leaderBoard = null;
            if (this.IsValidateGameMode(gameMode))
            {
                leaderBoard = await this.GetOrRequestLeaderBoardAsync(gameMode);
            }

            return leaderBoard;
        }

        public bool IsValidateGameMode(string gameMode)
        {
            var gameModes = this.GetAllowedGameModes();
            return gameModes.Any(x => x == gameMode);
        }

        private async Task<LeaderBoard> GetOrRequestLeaderBoardAsync(string gameMode)
        {
            var result = await this.leaderBoardRepository.GetLeaderBoardAsync(gameMode);
            if (result == null)
            {
                result = await this.updaterService.RequestOrUpdateLeaderBoardAsync(gameMode);
                await this.leaderBoardRepository.UpdateLeaderBoardAsync(result);
            }

            return result;
        }

        private IEnumerable<string> GetAllowedGameModes()
            => this.leaderBoardRepository.GetAvailableGameModes();
    }
}