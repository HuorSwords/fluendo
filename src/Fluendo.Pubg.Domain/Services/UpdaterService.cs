﻿namespace Fluendo.Pubg.Domain.Services.Services
{
    using Fluendo.Pubg.Domain.Contracts;
    using Fluendo.Pubg.Domain.Contracts.Agents;
    using Fluendo.Pubg.Domain.Models;
    using System.Threading.Tasks;

    public class UpdaterService : IUpdaterService
    {
        private readonly ILeaderBoardPubgAgent leaderBoardPubgApiClient;

        public UpdaterService(ILeaderBoardPubgAgent leaderBoardPubgApiClient)
        {
            this.leaderBoardPubgApiClient = leaderBoardPubgApiClient;
        }

        public Task<LeaderBoard> RequestOrUpdateLeaderBoardAsync(string gameMode)
        {
            return this.leaderBoardPubgApiClient.GetTop100PlayersAsync(gameMode);
        }
    }
}