﻿namespace Fluendo.Pubg.Domain.Models
{
    using System;
    using System.Collections.Generic;

    public sealed class LeaderBoard
    {
        public LeaderBoard()
        {
            this.Players = new List<PlayerRank>();
            this.ExpiresOn = DateTime.UtcNow.AddHours(4);
        }

        public string GameMode { get; set; }

        public IList<PlayerRank> Players { get; set; }

        public DateTime ExpiresOn { get; set; }
    }
}