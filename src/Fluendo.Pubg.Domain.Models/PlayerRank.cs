﻿namespace Fluendo.Pubg.Domain.Models
{
    using System;

    public sealed class PlayerRank
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public long Rank { get; set; }

        public DateTime ExpiresOn { get; set; }
    }
}