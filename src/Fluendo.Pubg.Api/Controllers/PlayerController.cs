﻿namespace Fluendo.Pubg.Api.Controllers
{
    using System;

    using Microsoft.AspNetCore.Mvc;

    using Fluendo.Pubg.Domain.Contracts.Services;

    [Route("[controller]")]
    [ResponseCache(Duration = 14400)]
    public class PlayerController : Controller
    {
        private readonly IPlayerStatsService playerStatsService;

        public PlayerController(IPlayerStatsService playerStatsService)
        {
            this.playerStatsService = playerStatsService ?? throw new ArgumentNullException(nameof(playerStatsService));
        }

        [HttpGet("stats/{accountid}")]
        public IActionResult GetByAccount(string accountid)
        {
            return this.Ok("value");
        }

        [HttpGet("stats/by-name/{playerName}")]
        public IActionResult GetByName(string playerName)
        {
            return this.Ok("value");
        }
    }
}