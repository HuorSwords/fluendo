﻿namespace Fluendo.Pubg.Api.Controllers
{
    using System;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Mvc;

    using Fluendo.Pubg.Api.Models;
    using Fluendo.Pubg.Api.Models.Extensions;
    using Fluendo.Pubg.Domain.Contracts.Services;

    [Route("[controller]")]
    [ResponseCache(Duration = 14400)]
    public class LeaderBoardController : Controller
    {
        private readonly ILeaderBoardService leaderBoardService;

        public LeaderBoardController(ILeaderBoardService leaderBoardService)
        {
            this.leaderBoardService = leaderBoardService ?? throw new ArgumentNullException(nameof(leaderBoardService));
        }

        [HttpGet("{gameMode}")]
        public async Task<IActionResult> Get(string gameMode)
        {
            LeaderBoardApiModel response = null;
            var isValid = this.leaderBoardService.IsValidateGameMode(gameMode);
            if (isValid)
            {
                var result = await this.leaderBoardService.GetLeaderBoardByModeAsync(gameMode);
                if (result == null)
                {
                    return this.Accepted();
                }

                response = result.ToApiModel();
            }

            return this.Ok(response);
        }
    }
}