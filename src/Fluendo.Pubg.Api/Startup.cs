﻿namespace Fluendo.Pubg.Api
{
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    using Swashbuckle.AspNetCore.Swagger;
    using MongoDB.Driver;

    using Fluendo.Pubg.Agents;
    using Fluendo.Pubg.Database.Repositories;
    using Fluendo.Pubg.Domain.Contracts;
    using Fluendo.Pubg.Domain.Contracts.Agents;
    using Fluendo.Pubg.Domain.Contracts.Repositories;
    using Fluendo.Pubg.Domain.Contracts.Services;
    using Fluendo.Pubg.Domain.Services;
    using Fluendo.Pubg.Domain.Services.Services;
    using Fluendo.Pubg.Database.Mongo.Contracts;
    using Fluendo.Pubg.Database.Mongo;
    using Fluendo.Pubg.Agents.Configurations;

    public class Startup
    {
        private const string WebApiName = "Fluendo.Pubg.Api";
        private const string WebApiVersion = "v1";
        private const string WebApiUrlEndpoint = "/swagger/v1/swagger.json";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // DI
            services
                .AddScoped<IMongoClient>(x => new MongoClient(this.Configuration.GetConnectionString("PubgDB")))
                .AddScoped<IMongoCollectionFactory, MongoCollectionFactory>(x => new MongoCollectionFactory(x.GetService<IMongoClient>(), "FluendoPubg"));

            services
                .AddScoped<ILeaderBoardRepository, LeaderBoardRepository>()
                .AddScoped<ILeaderBoardService, LeaderBoardService>()
                .AddScoped<IPlayerStatsService, PlayerStatsService>();

            services
                .AddScoped<IUpdaterService, UpdaterService>()
                .AddSingleton<ILeaderBoardPubgAgent, LeaderBoardPubgAgent>(provider =>
                        new LeaderBoardPubgAgent(this.Configuration.GetSection(nameof(PubgRestApiClientConfiguration)).Get<PubgRestApiClientConfiguration>()));

            // Swagger & MVC
            services
                .AddCors(options =>
                {
                    options.AddPolicy("LocalOnlyPolicy", builder =>
                    {
                        builder.WithOrigins("0.0.0.0");
                    });
                })
                .AddSwaggerGen(setup => setup.SwaggerDoc(WebApiVersion, new Info { Title = WebApiName, Version = WebApiVersion }))
                .AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection()
               .UseSwagger()
               .UseSwaggerUI(options => options.SwaggerEndpoint(WebApiUrlEndpoint, WebApiName))
               .UseMvc();
        }
    }
}