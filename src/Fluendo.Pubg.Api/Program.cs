﻿namespace Fluendo.Pubg.Api
{
    using System.Net;

    using Microsoft.AspNetCore;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;

    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    // Call additional providers here as needed.
                    // Call AddEnvironmentVariables last if you need to allow environment
                    // variables to override values from other providers.
                    config.AddEnvironmentVariables(prefix: "FLUENDO_");
                })
                .UseKestrel(options => {
					options.Listen(IPAddress.Parse("0.0.0.0"), 443, listenOptions =>
					{
                        listenOptions.UseHttps("fluendo.pfx", "Fluendo2019");
					});
                })                
                .UseStartup<Startup>();
    }
}