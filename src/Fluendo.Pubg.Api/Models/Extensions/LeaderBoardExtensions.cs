﻿namespace Fluendo.Pubg.Api.Models.Extensions
{
    using Fluendo.Pubg.Domain.Models;
    using System.Collections.Generic;

    public static class LeaderBoardExtensions
    {
        public static LeaderBoardApiModel ToApiModel(this LeaderBoard self)
        {
            return new LeaderBoardApiModel
            {
                GameMode = self.GameMode,
                Players = self.Players.ToApiModel()
            };
        }
    }

    public static class PlayerRankCollectionExtensions
    {
        public static IEnumerable<PlayerRankApiModel> ToApiModel(this IList<PlayerRank> self)
        {
            foreach (var item in self)
            {
                yield return new PlayerRankApiModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    Rank = item.Rank
                };
            }
        }
    }
}