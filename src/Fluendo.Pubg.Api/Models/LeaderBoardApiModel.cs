﻿namespace Fluendo.Pubg.Api.Models
{
    using System.Collections.Generic;

    public class LeaderBoardApiModel
    {
        public string GameMode { get; set; }

        public IEnumerable<PlayerRankApiModel> Players { get; set; }
    }
}