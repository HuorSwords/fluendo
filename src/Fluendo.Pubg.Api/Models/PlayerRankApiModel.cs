﻿namespace Fluendo.Pubg.Api.Models
{
    public class PlayerRankApiModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public long Rank { get; set; }
    }
}