﻿namespace Fluendo.Pubg.Agents.Entities
{
    public class LeaderBoardItemEntity
    {
        public string Type { get; set; }
        public string Id { get; set; }
        public LeaderBoardPlayerAttributesEntity Attributes { get; set; }
    }
}