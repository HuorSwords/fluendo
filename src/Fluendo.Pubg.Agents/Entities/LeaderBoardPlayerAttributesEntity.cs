﻿namespace Fluendo.Pubg.Agents.Entities
{
    public class LeaderBoardPlayerAttributesEntity
    {
        public string Name { get; set; }
        public int Rank { get; set; }
        public LeaderBoardPlayerAttributeStatsEntity Stats { get; set; }
    }
}