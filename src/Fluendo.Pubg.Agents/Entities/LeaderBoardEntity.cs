﻿namespace Fluendo.Pubg.Agents.Entities
{
    public class LeaderBoardEntity
    {
        public LeaderBoardData Data { get; set; }
        public LeaderBoardItemEntity[] Included { get; set; }
    }

    public class LeaderBoardData
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public LeaderBoardDataAttributes Attributes { get; set; }
    }

    public class LeaderBoardDataAttributes
    {
        public string ShardId { get; set; }
        public string GameMode { get; set; }
    }
}