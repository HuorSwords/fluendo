﻿namespace Fluendo.Pubg.Agents.Entities
{
    public class LeaderBoardPlayerAttributeStatsEntity
    {
        public long RankPoints { get; set; }
        public long Wins { get; set; }
        public long Games { get; set; }
        public float WinRatio { get; set; }
        public long AverageDamage { get; set; }
        public long Kills { get; set; }
        public float KillDeathRatio { get; set; }
        public float AverageRank { get; set; }
    }
}