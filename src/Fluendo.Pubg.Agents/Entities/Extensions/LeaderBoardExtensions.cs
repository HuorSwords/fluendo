﻿namespace Fluendo.Pubg.Agents.Entities.Extensions
{
    using System.Linq;

    using Fluendo.Pubg.Domain.Models;

    public static class LeaderBoardExtensions
    {
        public static LeaderBoard ToModel(this LeaderBoardEntity self)
        {
            var leaderBoard = new LeaderBoard
            {
                GameMode = self.Data.Attributes.GameMode
            };

            foreach (var player in self.Included.Select(x => new PlayerRank { Id = x.Id, Name = x.Attributes.Name, Rank = x.Attributes.Rank }))
            {
                leaderBoard.Players.Add(player);
            }

            return leaderBoard;
        }
    }
}