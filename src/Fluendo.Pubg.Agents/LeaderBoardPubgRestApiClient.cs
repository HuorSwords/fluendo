﻿namespace Fluendo.Pubg.Agents
{
    using System;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading;
    using System.Threading.Tasks;
    using Fluendo.Pubg.Agents.Configurations;
    using Fluendo.Pubg.Domain.Contracts.Agents;

    public abstract class PubgRestApiClient<TEntity> : IPubgApiClient<TEntity> where TEntity : class
    {
        private const int OneMinuteInMs = 3600000;

        private const string AuthenticationScheme = "bearer";

        private const string AcceptedMediaType = "application/vnd.api+json";
        
        private readonly PubgRestApiClientConfiguration configuration;

        private readonly Uri requestUri;

        private int requestCount = 0;

        protected PubgRestApiClient(PubgRestApiClientConfiguration configuration)
        {
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            if (!Uri.IsWellFormedUriString(configuration.RequestUri, UriKind.RelativeOrAbsolute))
            {
                throw new ArgumentException(nameof(configuration.RequestUri));
            }

            this.requestUri = this.CreateUriFor(configuration.RequestUri);
            this.ConfigureRestClient();
        }

        protected HttpClient Client { get; private set; }

        public async virtual Task<TEntity> GetAsync(string path)
            => await this.GetAsync(path, CancellationToken.None);

        public async virtual Task<TEntity> GetAsync(string path, CancellationToken cancellationToken)
        {
            requestCount++;
            if (requestCount > this.configuration.MaxAllowedRequest)
            {
                await Task.Delay(this.configuration.DelayInMilliseconds);
            }

            cancellationToken.ThrowIfCancellationRequested();

            var uri = string.IsNullOrEmpty(path)
                ? this.requestUri
                : this.CreateUriFor($"{this.requestUri}{path}");

            // TODO: Implement throttling to be proactive on requesting data from the server.
            var response = await Client.GetAsync(uri);
            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException)
            {
                await Task.Delay(OneMinuteInMs)
                          .ContinueWith(async x => await this.GetAsync(path, cancellationToken));
            }


            return await this.Deserialize(await response.Content.ReadAsStringAsync());
        }

        protected abstract Task<TEntity> Deserialize(string json);

        private Uri CreateUriFor(string requestUri)
            => Uri.IsWellFormedUriString(requestUri, UriKind.RelativeOrAbsolute)
                    ? new Uri(requestUri)
                    : throw new ArgumentException(nameof(requestUri));

        private void ConfigureRestClient()
        {
            this.Client = new HttpClient
            {
                BaseAddress = this.requestUri
            };

            this.Client.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse(AcceptedMediaType));
            this.Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(AuthenticationScheme, this.configuration.Token);
        }
    }
}