﻿namespace Fluendo.Pubg.Agents.Configurations
{
    public class PubgRestApiClientConfiguration
    {
        public string RequestUri { get; set; }

        public string Token { get; set; }

        public int MaxAllowedRequest { get; set; }

        public int DelayInMilliseconds { get; set; }
    }
}
