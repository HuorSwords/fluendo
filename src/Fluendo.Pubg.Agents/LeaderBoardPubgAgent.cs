﻿namespace Fluendo.Pubg.Agents
{
    using System.Linq;
    using System.Threading.Tasks;

    using Newtonsoft.Json;

    using Fluendo.Pubg.Domain.Contracts.Agents;
    using Fluendo.Pubg.Domain.Models;
    using Fluendo.Pubg.Agents.Entities;
    using Fluendo.Pubg.Agents.Entities.Extensions;
    using Fluendo.Pubg.Agents.Configurations;

    public class LeaderBoardPubgAgent : PubgRestApiClient<LeaderBoard>, ILeaderBoardPubgAgent
    {

        public LeaderBoardPubgAgent(PubgRestApiClientConfiguration configuration)
            : base(configuration)
        {
        }

        public async Task<LeaderBoard> GetTop100PlayersAsync(string gameMode)
        {
            LeaderBoard result = null;
            int pageNumber = 0;
            do
            {
                var leaderBoard = await this.GetAsync($"shards/steam/leaderboards/{gameMode}?page%5Bnumber%5D={pageNumber}");
                if (result == null)
                {
                    result = leaderBoard;
                }
                else
                {
                    var newPlayers = false;
                    foreach (var item in leaderBoard.Players)
                    {
                        if (result.Players.All(x => x.Id != item.Id))
                        {
                            result.Players.Add(item);
                        }
                    }

                    if (!newPlayers) break;
                }

                pageNumber++;
            } while (result.Players.Count < 100);

            result.Players = result.Players
                                   .OrderBy(x => x.Rank)
                                   .Take(100)
                                   .ToList();

            return result;
        }

        protected override Task<LeaderBoard> Deserialize(string json)
            => Task.FromResult(JsonConvert.DeserializeObject<LeaderBoardEntity>(json).ToModel());
    }
}