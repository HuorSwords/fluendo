﻿namespace Fluendo.Pubg.Database.Mongo.Models
{
    using System.Collections.Generic;

    using MongoDB.Bson;
    using MongoDB.Bson.Serialization.Attributes;

    public sealed class LeaderBoardDocument
    {
        [BsonId]
        public string GameMode { get; set; }

        [BsonElement()]
        public IList<PlayerRankDocument> Players { get; set; }
    }
}