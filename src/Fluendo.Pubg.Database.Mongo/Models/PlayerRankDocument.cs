﻿namespace Fluendo.Pubg.Database.Mongo.Models
{
    using MongoDB.Bson;
    using MongoDB.Bson.Serialization.Attributes;
    using System;

    public sealed class PlayerRankDocument
    {
        [BsonId]
        public string Id { get; set; }

        [BsonElement()]
        public string Name { get; set; }

        [BsonElement()]
        public long Rank { get; set; }

        [BsonElement()]
        public DateTime ExpiresOn { get; set; }
    }
}
