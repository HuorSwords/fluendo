﻿namespace Fluendo.Pubg.Database.Mongo.Models.Extensions
{
    using System.Linq;

    using Fluendo.Pubg.Domain.Models;

    public static class DomainModelsExtensions
    {
        public static LeaderBoard ToDomainModel(this LeaderBoardDocument self)
        {
            return new LeaderBoard
            {
                GameMode = self.GameMode,
                Players = self.Players.Select(x => x.ToDomainModel()).ToList()
            };
        }

        public static PlayerRank ToDomainModel(this PlayerRankDocument self)
        {
            return new PlayerRank
            {
                Id = self.Id,
                Name = self.Name,
                Rank = self.Rank,
                ExpiresOn = self.ExpiresOn
            };
        }

        public static LeaderBoardDocument ToDocument(this LeaderBoard self)
        {
            return new LeaderBoardDocument
            {
                GameMode = self.GameMode,
                Players = self.Players.Select(x => x.ToDocument()).ToList()
            };
        }

        public static PlayerRankDocument ToDocument(this PlayerRank self)
        {
            return new PlayerRankDocument
            {
                Id = self.Id,
                Name = self.Name,
                Rank = self.Rank,
                ExpiresOn = self.ExpiresOn
            };
        }
    }
}
