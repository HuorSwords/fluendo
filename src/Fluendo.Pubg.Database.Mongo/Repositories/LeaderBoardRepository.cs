﻿namespace Fluendo.Pubg.Database.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using MongoDB.Driver;

    using Fluendo.Pubg.Database.Mongo.Contracts;
    using Fluendo.Pubg.Database.Mongo.Models;
    using Fluendo.Pubg.Database.Mongo.Models.Extensions;
    using Fluendo.Pubg.Domain.Contracts.Repositories;
    using Fluendo.Pubg.Domain.Models;
    using MongoDB.Bson;

    public class LeaderBoardRepository : ILeaderBoardRepository
    {
        private readonly IMongoCollection<LeaderBoardDocument> collection;

        public LeaderBoardRepository(IMongoCollectionFactory factory)
        {
            if (factory == null)
            {
                throw new ArgumentNullException(nameof(factory));
            }

            this.collection = factory.CreateCollection<LeaderBoardDocument>();
        }

        public IEnumerable<string> GetAvailableGameModes() 
            => new string[] { "duo", "duo-fpp", "solo", "solo-fpp", "squad", "squad-fpp" };

        public async Task<LeaderBoard> GetLeaderBoardAsync(string gameMode)
        {
            var items = await this.collection.FindAsync(x => x.GameMode == gameMode);
            var result = await items.FirstOrDefaultAsync();
            return result?.ToDomainModel();
        }

        public async Task<LeaderBoard> UpdateLeaderBoardAsync(LeaderBoard leaderBoard)
        {
            var document = await this.collection.FindOneAndReplaceAsync(x => x.GameMode == leaderBoard.GameMode, leaderBoard.ToDocument());
            if (document == null)
            {
                this.collection.InsertOne(leaderBoard.ToDocument());
            }

            return await this.GetLeaderBoardAsync(leaderBoard.GameMode);
        }
    }
}