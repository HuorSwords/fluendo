﻿namespace Fluendo.Pubg.Database.Mongo
{
    using System;
    using System.Threading.Tasks;

    using MongoDB.Bson;
    using MongoDB.Driver;

    using Fluendo.Pubg.Database.Mongo.Contracts;

    public class MongoCollectionFactory : IMongoCollectionFactory
    {
        private readonly IMongoClient mongoClient;

        private readonly IMongoDatabase database;

        public MongoCollectionFactory(IMongoClient mongoClient, string databaseName)
        {
            this.mongoClient = mongoClient ?? throw new ArgumentNullException(nameof(mongoClient));
            this.database = this.mongoClient.GetDatabase(databaseName);
        }

        public IMongoCollection<TDocument> CreateCollection<TDocument>()
        {
            if (!this.CollectionExistsAsync(nameof(TDocument)).Result)
            {
                database.CreateCollection(nameof(TDocument));
            }

            Type type = typeof(TDocument);
            return database.GetCollection<TDocument>(type.Name);
        }

        private async Task<bool> CollectionExistsAsync(string collectionName)
        {
            var filter = new BsonDocument("name", collectionName);
            var collections = await this.database.ListCollectionsAsync(new ListCollectionsOptions { Filter = filter });
            return await collections.AnyAsync();
        }
    }
}