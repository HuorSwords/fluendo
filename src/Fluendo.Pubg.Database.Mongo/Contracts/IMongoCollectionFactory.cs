﻿namespace Fluendo.Pubg.Database.Mongo.Contracts
{
    using MongoDB.Driver;

    public interface IMongoCollectionFactory
    {
        IMongoCollection<TDocument> CreateCollection<TDocument>();
    }
}
