FROM microsoft/dotnet:sdk AS build-env
WORKDIR /app

# Copy everything else, restore and build
COPY . ./

RUN dotnet restore src/Fluendo.Pubg.Api/Fluendo.Pubg.Api.csproj
RUN dotnet publish src/Fluendo.Pubg.Api/Fluendo.Pubg.Api.csproj -c Release -o /app/out

# Build runtime image
FROM microsoft/dotnet:aspnetcore-runtime
WORKDIR /app
COPY --from=build-env /app/out .
COPY /cert/* .

EXPOSE 443
ENTRYPOINT ["dotnet", "Fluendo.Pubg.Api.dll"]