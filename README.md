# Fluendo Tech Test: PUBG API

## Features

- Swagger (done - partially as not authenticated)
- API endpoints (done)
- MongoDb (done)
- Redis (pending)
- PUBG third party API data retrieval
  - Dashboard (done - partially, as is invoked directly from the API until Host Service will be in place)
  - User by name (pending)
- Secure API (pending)
- Use Hosted Service to perform long running operations (pending)
- Demonstrating test / mocking knowledge (pending)
- Docker integration (done)
- Docker compose environment vars (done - partially)

## Instructions

To run the project inside the docker environment, you should execute this command on the directory where you have located the cloned repository.

```bash
docker-compose up --build
```

After doing that, you will be able to test the API using the endpoint https://localhost:5001/swagger

**DISCLAIMER**: Only the endpoing for retrieving Dashboard by game mode is working. The others endpoints are development pending.

## Environment variables

To change the environment variables used on the docker, you can edit the `.env` file in order to change some of the already existing variables.

In example, you can change the MongoDb connection string putting your desired value on the `FLUENDO_ConnectionStrings__PubgDB` property.

```json
FLUENDO_ConnectionStrings__PubgDB=mongodb://mongodb:27017
```

Any application related property modified in the `.env` file should be preceded by `FLUENDO_` prefix.